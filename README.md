## Playlist Manager

### Description

A mini-site that allow users to create playlists and share YouTube videos. (WIP)

![Playlist Manager](https://bitbucket.org/rodobo/playlist-manager/raw/master/PlaylistManager.gif "Playlist Manager App")

### Installation

* Clone this repo.
* In the project folder, run `npm install` or `yarn` to install all project dependencies.
* You'll need to setup a backend server with an API for this to run, similar to this [one](https://bitbucket.org/rodobo/playlist-manager-backend).
* Once you have the backend running, you'll need to update the API Endpoint.
* To launch the app, run `npm start` or `yarn start`.

### Tech Stack:
- [ReactJS](https://github.com/facebook/react/)

### Libraries:
- [Styled components](https://github.com/styled-components/styled-components)
- [MomentJS](https://github.com/moment/moment/)

### License

Submit any bugs by creating an issue or submitting a PR.
This repo is published under the MIT License.
