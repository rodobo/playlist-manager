import React from 'react';
import styled from 'styled-components';
import PlaylistListItem from '../PlaylistListItem';

const PlaylistListContainer = styled.div`
  width: 100%;
  justify-content: space-between;
`;

const PlaylistList = (props) => (
  <PlaylistListContainer>
    {
      props.playlists.map(playlist => (
        <div key={ playlist._id } >
          <PlaylistListItem playlist={ playlist } />
          <hr/>
        </div>
      ))
    }
  </PlaylistListContainer>
);

export default PlaylistList;
