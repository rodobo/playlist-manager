import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

const PlaylistListItemContainer = styled.div`
  width: 100%;
  padding: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const PlaylistDetailsLeft = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

const PlaylistDetailsTop = styled.div`
  display: flex;
  align-items: baseline;
  text-align: left;
`;

const PlaylistTitle = styled.div`
  font-size: 20px;
  font-weight: bold;
`;

const CreatedDate = styled.div`
  font-size: 14px;
  font-style: italic;
  margin-left: 10px;
`;

const UserDetails = styled.div`
  font-size: 14px;
  margin-top: 10px;
`;

const PlaylistArrowLink = styled.a`
  font-size: 40px;
  border: none;
  padding: 0;
  cursor: pointer;
  margin-right: 20px;
`;

const calcDate = (date) => {
  const dateMoment = moment(new Date(date));
  return moment(dateMoment).fromNow();
};

const PlaylistListItem = (props) => (
  <PlaylistListItemContainer>
    <PlaylistDetailsLeft>
      <PlaylistDetailsTop>
        <PlaylistTitle>{ props.playlist.name }</PlaylistTitle>
        <CreatedDate>Created { calcDate(props.playlist.creationDate) }</CreatedDate>
      </PlaylistDetailsTop>
      <UserDetails>{ props.playlist.createdBy }</UserDetails>
    </PlaylistDetailsLeft>
    <PlaylistArrowLink>></PlaylistArrowLink>
  </PlaylistListItemContainer>
);

export default PlaylistListItem;
