import React, { Component } from 'react';
import styled from 'styled-components';

import PlaylistList from '../PlaylistList';
import PlaylistForm from '../PlaylistForm';

const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const PlaylistListContainer = styled.div`
  padding: 20px;
`;

const CreatePlaylistButton = styled.button`
  width: 180px;
  padding: 14px;
  font-size: 14px;
  float: right;
`;

const FormModal = styled.div`
  width: 50%;
  min-width: 20rem;
  position: fixed;
  top: 25vh;
  left: 25vw;
  background-color: white;
  box-shadow: #333 0.5rem 0.5rem 1rem;
  border-radius: 1rem;
  padding: 2rem;
  z-index: 10;
`;

// TODO: Place api calls in separate library file
const getPlaylists = async () => {
  let playlists = [];
  await fetch('http://127.0.0.1:8080/playlist')
    .then(res => res.json())
    .then(data => {
      playlists = [...data];
    });
  return playlists;
};

const addNewPlaylist = async (playlist) => {
  return await fetch('http://127.0.0.1:8080/playlist', {
    method: 'post',
    body: JSON.stringify(playlist),
    headers: { 'Content-Type': 'application/json' },
  })
  .then(data => data.json())
  .then(res => {
    console.log(res);
    return res;
  });
};

class PlaylistManagerContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayForm: false,
      playlists: [],
    };
  }

  componentWillMount = () => {
    this.setPlaylists();
  }

  handleClickNewPlaylist = () => {
    this.setState(prevState => ({
      displayForm: !prevState.displayForm,
    }));
  }

  submitNewPlaylist = async (playlist) => {
    await addNewPlaylist(playlist)
    .then(res => {
      console.log('res', res);
      if (res) {
        this.setState({
          displayForm: false,
          playlists: [res, ...this.state.playlists],
        });
        return true;
      }
    });
  }

  setPlaylists = async () => {
    const playlists = await getPlaylists();
    this.setState(prevState => ({
      playlists: [...prevState.playlists, ...playlists],
    }));
  }

  render = () => (
    <div>
      <MainContainer>
        { !this.state.playlists.length &&
          <div>Loading...</div>
        }
        { (this.state.playlists.length > 0) &&
          <PlaylistListContainer>
            <CreatePlaylistButton onClick={this.handleClickNewPlaylist}>Create new Playlist</CreatePlaylistButton>
            <PlaylistList playlists={ this.state.playlists } />
          </PlaylistListContainer>
        }
      </MainContainer>
      { this.state.displayForm &&
        <FormModal>
          <PlaylistForm onSubmit={ (playlist) => this.submitNewPlaylist(playlist) } />
        </FormModal>
      }
    </div>
  );
}

export default PlaylistManagerContainer;
