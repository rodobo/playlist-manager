import React from 'react';
import Video from '../Video/';

const Playlist = (props) => (
  <div className="playlist-container">
    <h1>Playlist Title</h1>
    <p>Playlist description</p>
    <div className="playlist-details">
      <p className="video-user">user display name</p>
      <p className="created-date">Created date</p>
    </div>
    <button className="add-video">Add new video</button>
    <div className="video-list">
      <Video />
    </div>
  </div>
);

export default Playlist;
