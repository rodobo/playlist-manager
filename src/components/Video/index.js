import React from 'react';

const Video = (props) => (
  <div className="video-container">
    <div className="video-embed">screenshot</div>
    <div className="video-details-container">
      <p className="video-user">user display name</p>
      <p className="video-link">youtube link</p>
      <p className="video-description">description</p>
    </div>
  </div>
);

export default Video;
