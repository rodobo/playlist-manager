import React, { Component } from 'react';
import styled, { css } from 'styled-components';

const FormContainer = styled.div`
  width: 80%;
  display: flex;
  flex-direction: column;
`;

const FormFieldLabel = styled.label`
  font-weight: bold;
`;

const FormTextInput = styled.input`
  margin-bottom: 1rem;
  ${props => {
    return !props.value.trim() && props.isRequired && props.isMissing && css`
    border-color: red;
  `}};
`;

const FormTextArea = styled.textarea`
  margin-bottom: 1rem;
`;

const FormSubmitButton = styled.button`
  padding: 0.8rem;
`;

const FormErrorMsg = styled.p`
  display: none;
  ${props => props.show && css`
    display: block;
    color: red;
  `};
`;

class PlaylistForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      playlistName: '',
      description: '',
      showErrorMsg: false,
    }
  }

  handleClickAddPlaylist = () => {
    const {username, playlistName, description} = this.state;
    if (username.trim() && playlistName.trim()) {
      this.props.onSubmit({
        createdBy: username,
        name: playlistName,
        description,
      });
    } else {
      this.setState({
        showErrorMsg: true,
      });
    }
  }

  render() {
    return (
      <FormContainer>
        <FormErrorMsg show={ this.state.showErrorMsg }>Please enter values in all required fields!</FormErrorMsg>
        <FormFieldLabel for="username" isMissing={ this.state.showErrorMsg } isRequired={ true }>Your Name*</FormFieldLabel>
        <FormTextInput
          id="username"
          isMissing={ this.state.showErrorMsg }
          isRequired={ true }
          onChange={ (event) => this.setState({ username: event.target.value, showErrorMsg: false }) }
          value={ this.state.username }
        />
        <FormFieldLabel for="playlist-name">Name of Playlist*</FormFieldLabel>
        <FormTextInput
          id="playlist-name"
          isMissing={ this.state.showErrorMsg }
          isRequired={ true }
          onChange={ (event) => this.setState({ playlistName: event.target.value, showErrorMsg: false }) }
          value={ this.state.playlistName }
        />
        <FormFieldLabel for="description">Description</FormFieldLabel>
        <FormTextArea id="description" onChange={ (event) => this.setState({ description: event.target.value }) }/>
        <FormSubmitButton onClick={ this.handleClickAddPlaylist }>Add Playlist</FormSubmitButton>
      </FormContainer>
    );
  }
}

export default PlaylistForm;
