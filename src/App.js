import React, { Component } from 'react';
import PlaylistManagerContainer from './components/PlaylistManagerContainer';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Playlist Manager</h1>
        <PlaylistManagerContainer />
      </div>
    );
  }
}

export default App;
